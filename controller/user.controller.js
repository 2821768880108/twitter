'use strict'

var bcrypt = require('bcrypt-nodejs');
var jwt = require('../services/jwt');
var User = require('../model/user.model');
var Tweet = require('../model/tweet.model');


function saveUser(req, res) {
    var user = new User();
    var params = req.body;

    if (params.command.split(' ')[1] && params.command.split(' ')[2], params.command.split(' ')[3],
        params.command.split(' ')[4], params.command.split(' ')[5]) {
        User.findOne({ $or: [{ username: params.command.split(' ')[3] }, { email: params.command.split(' ')[4] }] }, (err, userFound) => {
            if (err) {
                res.status(500).send({ message: 'Error general, porfavor intentelo mas tarde' });
            } else if (userFound) {
                res.status(200).send({ message: 'usuario o correo ya utilizado' });
            } else {
                user.name = params.command.split(' ')[1];
                user.lastname = params.command.split(' ')[2];
                user.username = params.command.split(' ')[3];
                user.email = params.command.split(' ')[4];

                bcrypt.hash(params.command.split(' ')[5], null, null, (err, passwordHash) => {
                    if (err) {
                        res.status(500).send({ message: 'Se a generado un error al encriptar la contraseña' });
                    } else if (passwordHash) {
                        user.password = passwordHash;
                        user.save((err, saveUser) => {
                            if (err) {
                                res.status(500).send({ message: 'Se produjo un error al guardar usuario' })
                            } else if (saveUser) {
                                res.status(200).send({ message: 'Usuario creado', saveUser });
                            } else {
                                res.status(418).send({ message: 'Usuario no guardado' })
                            }
                        })
                    } else {
                        res.status(418).send({ message: 'Error inesperado, intentelo más tarde' });
                    }
                });
            }
        });
    } else {
        res.status(404).send({ message: 'Porfavor ingrese todos los datos(nombre, apellido, usuario, email, contraseña)' });
    }
}

function login(req, res) {
    var params = req.body;
    var command = params.command;

    if (command.split(' ')[1] && command.split(' ')[2]) {
        User.findOne({ $or: [{ email: command.split(' ')[1] }, { username: command.split(' ')[1] }, { name: command.split(' ')[1] }] }, (err, check) => {
            if (err) {
                res.status(500).send({ message: 'Se a producido un error al generar la petición' })
            } else if (check) {
                bcrypt.compare(command.split(' ')[2], check.password, (err, passwordOk) => {
                    if (err) {
                        res.status(500).send({ message: 'Se a producido un error al comparar contraseñas' });
                    } else if (passwordOk) {
                        if (params.gettoken = true) {
                            res.send({ token: jwt.createToken(check), user: check.name });
                        } else {
                            res.send({ message: 'Bienvenido' });
                        }
                    } else {
                        res.send({ message: 'Contraseña incorrecta porfavor intentelo de nuevo' })
                    }
                });
            } else {
                res.send({ message: 'Porfavor verificar que los datos del usuario sean correctos' });
            }
        });
    } else {
        res.send({ message: 'Debes ingresar tu correo o username y contraseña ' });
    }
}


function addTweet(req, res) {
    var tweet = new Tweet();
    var command = req.body.command;

    if (req.headers.authorization) {
        if (command.split(' ')[1] && command.split(' ').length < 50) {
            var textTweet = '';
            for (let i = 1; i <= 50; i++) {
                if (command.split(' ')[i] != null) {
                    var textTweet = textTweet + command.split(' ')[i] + ' ';
                } else if (command.split(' ')[i] == null) {
                    i = 50;
                }
            }
            tweet.text = textTweet;
            tweet.user = req.user.sub;

            tweet.save((err, tweetPost) => {
                if (err) {
                    res.status(500).send({ message: 'Se a producido un error al generar la petición' });
                } else if (tweetPost) {
                    res.send({ message: 'El tweet a sido publicado', tweetPost });
                } else {
                    res.status(418).send({ message: 'El tweet no se a podido publicar' })
                }
            });
        } else {
            res.send({ message: 'Porfavor debes de ingresar la información correcta' })
        }
    } else {
        res.send({ message: 'Debe iniciar sesión' })
    }
}


function editTweet(req, res) {
    var command = req.body.command;
    var textTweet = '';

    if (req.headers.authorization) {
        for (let i = 2; i <= 50; i++) {
            if (command.split(' ')[i] != null) {
                var textTweet = textTweet + command.split(' ')[i] + ' ';
            } else if (command.split(' ')[i] == null) {
                i = 50;
            }
        } Tweet.findById(command.split(' ')[1], (err, findTweet) => {
            if (err) {
                res.status(418).send({ message: 'El tweet que buscas no a sido publicado en tu perfil' })
            } else if (findTweet) {
                if (findTweet.user == req.user.sub) {
                    Tweet.findByIdAndUpdate(command.split(' ')[1], { text: textTweet }, { new: true }, (err, tweetUpdated) => {
                        if (err) {
                            res.status(500).send({ message: 'Se a producido un error al generar la petición' });
                        } else if (tweetUpdated) {
                            res.status(200).send({ message: 'El tweet a sido acutalizado correctamente', tweetUpdated });
                        } else {
                            res.status(418).send({ message: 'El tweet no se a podido publicar' })
                        }
                    })
                } else if (findTweet.user != req.user.sub) {
                    res.status(403).send({ message: 'No tienes acceso para la acción que deseas realizar' })
                }
            } else {
                res.status(418).send({ message: 'Debe buscar un tweet el cual editar' })
            }
        });
    } else {
        res.send({ message: 'Debe iniciar sesión' })
    }
}


function viewTweets(req, res) {
    if (req.headers.authorization) {
        Tweet.find({}, (err, tweets) => {
            if (err) {
                res.status(500).send({ message: 'Se a producido un error al generar la petición' });
            } else if (tweets) {
                res.status(200).send({ message: 'Los tweets que has publicado son los siguientes:', tweets });
            } else {
                res.status(418).send({ message: 'El tweet no se a podido publicar' })
            }
        })
    } else {
        res.send({ message: 'Debe iniciar sesión' })
    }
}


function deleteTweet(req, res) {
    var command = req.body.command;

    if (req.headers.authorization) {
        Tweet.findById(command.split(' ')[1], (err, findTweet) => {
            if (err) {
                res.status(418).send({ message: 'El tweet no se a podido publicar' })
            } else if (findTweet) {
                if (findTweet.user == req.user.sub) {
                    Tweet.findByIdAndDelete(command.split(' ')[1], (err, tweetDeleted) => {
                        if (err) {
                            res.status(500).send({ message: 'Se a producido un error al generar la petición' });
                        } else if (tweetDeleted) {
                            res.status(200).send({ message: 'El tweet a sido eliminado correctamente', tweetDeleted });
                        } else {
                            res.status(418).send({ message: 'El tweet no se a podido publicar' })
                        }
                    });
                } else if (findTweet.user != req.user.sub) {
                    res.status(403).send({ message: 'No tienes acceso para la acción que deseas realizar' })
                }
            } else {
                res.status(418).send({ message: 'El tweet no se a podido publicar' })
            }
        });
    } else {
        res.send({ message: 'Debe iniciar sesión' })
    }
}


function follow(req, res) {
    var command = req.body.command;

    if (req.headers.authorization) {
        User.findById(req.user.sub, (err, findUser) => {
            if (err) {
                res.status(500).send({ message: 'Se a producido un error al buscar usuario' });
            } else if (findUser) {
                var followedUser = false;
                for (var i = 0; i < findUser.followers.length; i++) {
                    if (findUser.followers[i] == command.split(' ')[1]) {
                        followedUser = true;
                        i = findUser.followers.length;
                    }
                } if (followedUser == false) {
                    User.findByIdAndUpdate(req.user.sub, { $push: { followers: command.split(' ')[1] } }, { new: true }, (err, following) => {
                        if (err) {
                            res.status(500).send({ message: 'Se a producido un error al intentar seguir usuario' });
                        } else if (following) {
                            res.status(200).send({ message: 'Ahora ya sigues a este usuario' + command.split(' ')[1] })
                        } else {
                            res.status(404).send({ message: 'No se obtuvieron los datos necesarios' });
                        }
                    });
                } else if (followedUser == true) {
                    res.status(200).send({ message: 'Este usuario ya lo sigues' })
                }
            } else {
                res.status(418).send({ message: 'El tweet no se a podido publicar' })
            }
        });
    } else {
        res.send({ message: 'Debe iniciar sesión' })
    }
}


function profile(req, res) {
    var command = req.body.command;
    if (req.headers.authorization) {
        User.findOne({ username: command.split(' ')[1] }, (err, findProfile) => {
            if (err) {
                res.status(500).send({ message: 'Se a producido un error al intentar seguir usuario' });
            } else if (findProfile) {
                res.status(200).send({ message: findProfile })
            } else {
                res.status(404).send({ message: 'El usuario que buscas no existe' });
            }
        });
    } else {
        res.send({ message: 'Debe iniciar sesión' })
    }
}


function unfollow(req, res) {
    var command = req.body.command;

    if (req.headers.authorization) {
        User.findByIdAndUpdate(req.user.sub, { $pull: { followers: command.split(' ')[1] } }, { new: true }, (err, following) => {
            if (err) {
                res.status(500).send({ message: 'Se a producido un error al intentar seguir usuario' });
            } else if (following) {
                res.status(200).send({ message: 'Has dejado de seguir a este usuario ' + command.split(' ')[1] })
            } else {
                res.status(404).send({ message: 'El usuario que deseas dejar de seguir no existe' });
            }
        });
    } else {
        res.send({ message: 'Debe iniciar sesión' })
    }
}



function command(req, res) {
    var params = req.body;
    var command = params.command.split(' ')[0];

    if (command == 'ADD_TWEET') {
        addTweet(req, res);
    } else if (command == 'LOGIN') {
        login(req, res);
    } else if (command == 'REGISTER') {
        saveUser(req, res);
    } else if (command == 'DELETE_TWEET') {
        deleteTweet(req, res);
    } else if (command == 'EDIT_TWEET') {
        editTweet(req, res);
    } else if (command == 'VIEW_TWEETS') {
        viewTweets(req, res);
    } else if (command == 'FOLLOW') {
        follow(req, res);
    } else if (command == 'UNFOLLOW') {
        unfollow(req, res);
    } else if (command == 'PROFILE') {
        profile(req, res);
    } else {
        res.status(404).send({ message: 'Revisa que el comando este escrito correctamente' });
    }
}


module.exports = {
    command
}